import cdsapi
from datetime import date

################ User ################ 
date_start      = date(1986,4,25)
date_end        = date(1986,4,30)
resolution      = 0.25
lonmin, lonmax  = -30,50
latmin, latmax  = 30,75
step            = 1
###################################### 

hr_list = ["{:02d}".format(i) for i in range(0,24,step)]

#Parameters
params = { 'format': 'netcdf' }

#Common
params['grid'] = "{res}/{res}".format(res=resolution)
params['area'] = "{latmax}/{lonmin}/{latmin}/{lonmax}".format(lonmin=lonmin,
                                                              lonmax=lonmax,
                                                              latmin=latmin,
                                                              latmax=latmax)
params['class']    = 'ea'
params['expver']   = '1'
params['stream']   = 'oper'
params['type']     = 'an'
params['time']     = "/".join(hr_list)
params['date']     = "{date1}/to/{date2}".format(date1=date_start.strftime("%Y-%m-%d"),
                                                 date2=date_end.strftime("%Y-%m-%d"))
params['levtype']  = 'ml'
params['param']    = '129/130/131/132/133/135/152'
params['levelist'] = '1/to/137'

c = cdsapi.Client()
c.retrieve('reanalysis-era5-complete',
            params,
           'chernobyl1986-ml-19860425_19860430.nc')

