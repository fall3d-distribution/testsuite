import cdsapi
from datetime import date

################ User ################ 
date_start      = date(1986,4,25)
date_end        = date(1986,4,30)
resolution      = 0.25
lonmin, lonmax  = -30,50
latmin, latmax  = 30,75
step            = 1
###################################### 

#Parameters
params = { 'format': 'netcdf' }

#Common
params['grid'] = "{res}/{res}".format(res=resolution)
params['area'] = "{latmax}/{lonmin}/{latmin}/{lonmax}".format(lonmin=lonmin,
                                                              lonmax=lonmax,
                                                              latmin=latmin,
                                                              latmax=latmax)
params['product_type']   =  'reanalysis'
params['time']           = ["{hour:02d}:00".format(hour=i) for i in range(0,24,step)]
params['year']           = date_start.strftime("%Y")
params['month']          = date_start.strftime("%m")
params['day']            = ["{day:02d}".format(day=i) for i in range(date_start.day,date_end.day+1)]
params['variable']       = [ '10m_u_component_of_wind',
                             '10m_v_component_of_wind',
                             '2m_dewpoint_temperature',
                             '2m_temperature',
                             'boundary_layer_height',
                             'friction_velocity',
                             'land_sea_mask',
                             'mean_sea_level_pressure',
                             'orography',
                             'soil_type',
                             'surface_pressure',
                             'total_precipitation',
                             'volumetric_soil_water_layer_1'
                            ]

c = cdsapi.Client()
c.retrieve('reanalysis-era5-single-levels',
            params,
           'chernobyl1986-sfc-19860425_19860430.nc')

