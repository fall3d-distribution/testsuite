#!/usr/bin/env python3

import pandas as pd
import os
import sys

####### Inputs #######
case = "chernobyl-1986"
path = "../../Runs/"
out_file = "validation_chernobyl.csv"
obs_file = "track-points-55-REM.dat"

####### Check for python 3 version #######
if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

####### #######
def read_deposit(places, species):
    fnames = ["{case}.{place}.{species}.grnd.res".format(case=case,
                                                         place=place,
                                                         species=species) 
              for place in places]
    fnames = [os.path.join(path,fname) for fname in fnames]

    columns      = ['diameter',
                    'phi',
                    'load',
                    'fraction',
                   ]

    args = dict(skiprows         = 6,
                delim_whitespace = True,
                names            = columns,
               )

    df = pd.concat((pd.read_csv(f,**args) for f in fnames),
                    keys=places,
                    names=["id"],
                   )

    df = df.groupby(level=0).sum()
    return df

####### Read observation data ####### 
columns = ['lon', 'lat', 'id',
           'cs134-obs', 'cs137-obs', 'i131-obs',
           'country', 'city',
          ]
args = dict(delim_whitespace = True,
            index_col        = 'id',
            na_values        = '---',
            names            = columns,
           )
df = pd.read_csv(obs_file,**args) 

####### Read FALL3D output ####### 
df2 = read_deposit(df.index,'Cs134')
df['cs134-mod'] = df2['load']

df2 = read_deposit(df.index,'Cs137')
df['cs137-mod'] = df2['load']

df2 = read_deposit(df.index,'I131')
df['i131-mod'] = df2['load']

####### Save output file ####### 
print("Writing output file: {}".format(out_file))
df.to_csv(out_file, index_label = "id")
print("Done!")
