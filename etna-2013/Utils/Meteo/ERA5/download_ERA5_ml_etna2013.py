#!/usr/bin/env python
import cdsapi
 
c = cdsapi.Client()
c.retrieve('reanalysis-era5-complete', {
    'class'   : 'ea',
    'expver'  : '1',
    'stream'  : 'oper',
    'type'    : 'an',
    'param'   : '129/130/131/132/133/152',
    'levtype' : 'ml',
    'levelist': '1/to/137',
    'date'    : '2013-02-23/to/2013-02-25',
    'time'    : '00/01/02/03/04/05/06/07/08/09/10/11/12/13/14/15/16/17/18/19/20/21/22/23',
    'area'    : '55/0/25/40',
    'grid'    : '0.25/0.25',
}, '2013-02-23_71h_ml.grib2')
