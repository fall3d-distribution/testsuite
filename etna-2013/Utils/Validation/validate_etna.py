#!/usr/bin/env python3

import pandas as pd
import glob
import os
import sys

####### Inputs #######
case = "etna-2013"
path = "../../Runs/"
pts_file = "{case}.pts".format(case=case)
out_file = "validation_etna.csv"

####### Check for python 3 version #######
if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

####### Observations ####### 
data = {
        'phi_obs':  [-3.5, -4.0, -4.0, -3.5, -2.5, -1.5, -3.0, 1.0, 2.0, 3.0],
        'load_obs': [2.1E1, 5.9, 5.5, 2.2E1, 3.2E1, 5.2, 1.2, 2.9E-1, 1.3E-2, 1.4E-3],
        }
cities = [ "01-Baracca",
           "02-Casetta",
           "03-Bivio",
           "04-Forestale",
           "05-Chalet",
           "06-Castiglione",
           "07-Linguaglossa",
           "08-Messina",
           "09-Cardinale",
           "10-Brindisi",
         ]
df = pd.DataFrame(data, index=cities)

####### Load FALL3D output files ####### 
fnames = ["{case}.{city}.tephra.grnd.res".format(case=case,city=city) for city in cities]
fnames = [os.path.join(path,fname) for fname in fnames]
args   = dict(skiprows         = 6,
              delim_whitespace = True,
              names            = ['diameter', 'phi', 'load', 'fraction'],
             )
df_mod = pd.concat((pd.read_csv(fname,**args) for fname in fnames),
                    keys=cities,
                    names=["cities"],
                  )
df["load_mod"] = df_mod.load.groupby('cities').sum()
df["phi_mod"]  = df_mod.phi.loc[df_mod.groupby("cities").fraction.idxmax()].reset_index(level=1,drop=True)

print("Writing output file: {}".format(out_file))
df.to_csv(out_file, index_label = "cities")
print("Done!")
